# publicabot

A literate Mastodon bot.

Publicabot remembers the latest entries from arXiv (http://arxiv.org/) and will
give them back to you provided you give a category in a message sent to the
bot.

### Dependencies

- php ^7.1.2
- python ^3.4
- curl
- php modules:
  - ext-zip
  - ext-mbstring
  - ext-curl
  - ext-xml
  - ext-redis
  - ext-sqlite3/ext-mysql

Services:

- redis
- cron (although you can perfectly use another scheduler to run the commands)

### How to install

1. Clone the repository.

2. In the newly cloned directory, run `composer install`.

3. Copy `.env.example` in `.env`. 
   Edit `.env` to fill in your application token (see [guide to create one](./INSTALL.md)) and instance name. 

4. Initialize database (it's a SQLite database by default so you don't have anything to do, but MySQL/MariaDB is supported too) and Redis cache:

```
bin/console install
```

5. Crontab

The last command should have generated a `publicabot.cron` file for you with most default commands running. Install them as cron tasks via `crontab publicabot.cron`.
