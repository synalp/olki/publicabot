### Creating the Mastodon application

a. From the website, click on preferences and then Development<br/>
![](img/pic01.png)<br/><br/>
b. Click on "NEW APPLICATION"<br/>
![](img/pic02.png)<br/>
c. Fill the form<br/>
![](img/pic03.png) (make sure the app is on read + write which are default values)<br/>
d. Get your access token<br/>
![](img/pic04.png)<br/>

### Setting your token in the configuration

Edit `.env` and change these values
<br/><br/>
`app_host=HOST_NAME_OF_THE_ACCOUNT`<br/>
`app_token=YOUR_TOKEN_HERE`
<br/><br/>
`app_host` is the domain of the instance hosting the bot account<br/>
`app_token` is the string that you get previously (access token)