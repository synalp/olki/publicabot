<?php

namespace App\Repository;

use App\Entity\NotificationsLogs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method NotificationsLogs|null find($id, $lockMode = null, $lockVersion = null)
 * @method NotificationsLogs|null findOneBy(array $criteria, array $orderBy = null)
 * @method NotificationsLogs[]    findAll()
 * @method NotificationsLogs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotificationsLogsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, NotificationsLogs::class);
    }

    // /**
    //  * @return NotificationsLogs[] Returns an array of NotificationsLogs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NotificationsLogs
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
