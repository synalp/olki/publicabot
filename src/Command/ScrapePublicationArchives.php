<?php

namespace App\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class ScrapePublicationArchives extends Command
{
    use LockableTrait;

    protected static $defaultName = 'get:papers';
    private $container;
    private $doctrine;
    private $arxivCategories;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
        $this->doctrine = $container->get('doctrine');
        $this->arxivCategories = array('cs', 'physics', 'math', 'q-bio', 'q-fin', 'stat', 'eess', 'econ');
    }

    protected function configure()
    {
        $this->setDescription('Get the latest papers for arXiv')
             ->setHelp('It will dump everything in the corresponding Redis server, so make sure to edit your .env if your Redis install is not using the default connection port and host.')
             ->addOption('days', 'd', InputArgument::OPTIONAL, 'Days to get from today', 7);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        $io = new SymfonyStyle($input, $output);
        $io->title('arXiv papers');

        chdir( $this->container->getParameter('kernel.project_dir') . '/bin/arxivtoredis' );
        foreach ($this->arxivCategories as $category) {
            $io->note('getting new publications for category \''.$category.'\'');
            $this->liveExecuteCommand('env/bin/python3 -m arxivtoredis arxiv '.$category.' '.$input->getOption('days'));
            $this->liveExecuteCommand('env/bin/python3 -m arxivtoredis stats '.$category);
        }
    }

    /**
     * Execute the given command by displaying console output live to the user.
     *  @param  string  cmd          :  command to be executed
     *  @return array   exit_status  :  exit status of the executed command
     *                  output       :  console output of the executed command
     */
    protected function liveExecuteCommand($cmd)
    {

        while (@ ob_end_flush()); // end all output buffers if any

        $proc = popen("$cmd 2>&1 ; echo Exit status : $?", 'r');

        $live_output     = "";
        $complete_output = "";

        while (!feof($proc))
        {
            $live_output     = fread($proc, 10000);
            $complete_output = $complete_output . $live_output;
            echo "$live_output";
            @ flush();
        }

        pclose($proc);

        // get exit status
        preg_match('/[0-9]+$/', $complete_output, $matches);

        // return exit status and intended output
        return array (
            'exit_status'  => intval($matches[0]),
            'output'       => str_replace("Exit status : " . $matches[0], '', $complete_output)
        );
    }
}
