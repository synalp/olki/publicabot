<?php

namespace App\Command;


use App\Entity\NotificationsLogs;
use Curl\Curl;
use Predis;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Psr\Log\LogLevel;
use Clue\Arguments;


class ReadNotificationsCommand extends Command
{
    protected static $defaultName = 'read:notifications';
    private $container;
    private $doctrine;
    private $arxivCategories;
    private $app_token;
    private $app_host;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
        $this->doctrine = $container->get('doctrine');
        $this->arxivCategories = array('cs', 'physics', 'math', 'q-bio', 'q-fin', 'stat', 'eess', 'econ');
        $this->app_token = null;
        $this->app_host = null;
    }

    protected function configure()
    {
        $this->setDescription('Read the notification timeline of the account set in env')
             ->setHelp('Make sure you created a bot account from the web ui of Mastodon, and that you created an application from this account.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
	$verbosityLevelMap = [
	    LogLevel::NOTICE => OutputInterface::VERBOSITY_NORMAL,
	    LogLevel::INFO   => OutputInterface::VERBOSITY_NORMAL,
	];
	$logger = new ConsoleLogger($output, $verbosityLevelMap);
	
        $app_token = getenv("app_token");
        $app_host = getenv("app_host");

        $this->app_token = getenv("app_token");
        $this->app_host = getenv("app_host");

	//Retrieve maxium characters per toot
	$maxTootChars = $this->getMaxTootChars();

        //Retrieve the last read notification id
        $notificationLogs = $this->doctrine->getRepository("App:NotificationsLogs")->findOneBy([],["date" => "DESC"]);

        //Initialize a connection to the Redis SNC service, to query Redis for paper collected during with get:papers
        $redis = new Predis\Client();

        try {
            $curl = new Curl();
            $curl->setHeader('Authorization', 'Bearer '. $app_token);
            //Exclude favourites, follows and reblogs (the social network must accept these parameters)
            //Otherwise, you have to apply the filter in the API reply
            $params = "exclude_types[]=favourite&exclude_types[]=follow&exclude_types[]=reblog";
            //There was a previous call, so the min_id is set to this value
            if ($notificationLogs)
                $params .= "&min_id=".$notificationLogs->getLastReadId();
            //The get call to the API with parameters
            $url = "https://" . $app_host . "/api/v1/notifications?" . $params;
            $notifications = $curl->get($url);
            //Initialize the min_id value
            $min_id = null;
            $response = $notifications->response;
            //The json reply is turned into an array
            $responseArray = \json_decode($response, true);

            //Loop through header reply to find the min_id value that will be recorded for next calls
            if( $notifications->response_headers && count($responseArray) > 0){
                foreach ($notifications->response_headers as $value){
	    	    $logger->debug($value);
                    if( strpos($value, 'Link: ') !== false || strpos($value, 'link: ') !== false){
                        preg_match(
                            "/min_id=([0-9a-zA-Z]{1,})/",
                            $value,
                            $matches
                        );
                        if ($matches) {
                            $min_id = $matches[1];
                        }
                    }
                }

                //Initialize a NotificationsLogs entity to persist it
                $notificationLogs = new NotificationsLogs();
                $notificationLogs->setDate(new \DateTime());
		$notificationLogs->setLastReadId($min_id);
		$notificationLogs->setNewNotifications(count($responseArray));
		$this->doctrine->getManager()->persist($notificationLogs);
		$this->doctrine->getManager()->flush();

                /////////////////////////////////////
                /// Loop through unread notifications
                /////////////////////////////////////
                foreach ($responseArray as $singleNotification) {
                    $account = $singleNotification['account'];
                    $status = $singleNotification['status'];
                    $content = $status['content'];
                    $error_message = null;
		    $messages = array();
		    $messageCount = 0;
                    $tags = $status['tags'];

                    /////////////////////////////////////
                    /// Detect input
                    /////////////////////////////////////

                    $content = $this->cleanHTML($content); // removing html tags, hashtags and at-mentions to only leave the commands
                    $args = Arguments\split($content);

                    /////////////////////////////////////
                    /// Beginning with special commands
                    /////////////////////////////////////

                    //An account asks for removing their data in db
                    if ( count($tags) == 0 && (strpos(strtolower($content), 'remove') !== false ||  strpos(strtolower($content), 'delete') !== false)){
                        $messages[$messageCount] = $this->delAccountData($account);

                    //An account asks for help
                    } else if( count($tags) == 0 && (strpos(strtolower($content), 'help') !== false )){
                        $messages[$messageCount] =  "Hello @".$account['acct'].", here is how I work: \r\n\r\n";
                        $messages[$messageCount] .= "To get the latest papers:\r\n- mention me with a query: an arXiv category ('cs', 'physics', 'math', 'q-bio', 'q-fin', 'stat', 'eess', 'econ')\r\n- optionally add the number of papers you want\r\n\r\n";
                        //TODO $messages[$messageCount] .= "For subscription:\r\n- mention me your query only preceded by a time (ex: 0800 for 8am CET)\r\n\r\n";
                        //$messages[$messageCount] .= "For removing your data:\r\n- mention me with \"delete\" or \"remove\"";

                    //An account asks for stats
                    } else if( count($tags) == 0 && (strpos(strtolower($content), 'stats') !== false )){
                        //$numberInstances = $this->doctrine->getRepository("App:Instance")->countEntries();
                        //$numberTags = $this->doctrine->getRepository("App:Tag")->countEntries();
                        $messages[$messageCount] =  "Hello @".$account['acct']." here are some stats: \r\n\r\n";
                        //$messages[$messageCount] .= "I know ".$numberInstances." instances\r\n";
                        //$messages[$messageCount] .= "I know ".$numberTags." tags\r\n\r\n";
                        $tags = $this->doctrine->getRepository("App:Instance")->mostUsedTags();
                        $messages[$messageCount] .= "Most used tags: ";
                        foreach ($tags as $tag){
                            $messages[$messageCount] .= "#".$tag['tag']." ";
                        }

                    /////////////////////////////////////////////
                    /// Commands, beginning with known categories
                    /////////////////////////////////////////////
                    } else if ( in_array(preg_replace('~[[:cntrl:]]~', '', $args[0]), $this->arxivCategories) ) {
                        $category = preg_replace('~[[:cntrl:]]~', '', $args[0]);

                        try {
                            $redis->ping();
                            $redis->get('arxiv:sortedids:'.$category);
                        } catch (Exception $e) {
                            $logger->error('redis service is unavailable /!\\');
                        }

                        $sortedids = explode(' ', $redis->get('arxiv:sortedids:'.$category)); // we first get ids of latest papers in a variable where they are already stored sorted

                        // we check the second parameter, if any, is a number over 0
                        if ( count($args) === 2 && preg_match('/^\d+$/', $args[1]) && $args[1] > 0 ) {
                            // we slice the array to the limit it to the number of papers
                            $sortedids = array_slice($sortedids, 0, $args[1]);
                            $messages[$messageCount] = "@".$account['acct']." here are the ".$args[1]." most recently updated articles:\r\n";
                        } else {
                            $sortedids = array_slice($sortedids, 0, 5);
                            $messages[$messageCount] = "@".$account['acct']." here are the 5 most recently updated articles:\r\n";
                        }

                        // we get the papers with their ids
                        $latestPapers = array();
                        foreach ($sortedids as $id) {
                            $latestPapers[] = json_decode($redis->get('arxiv:raw:'.$id));
                        }

                        foreach ($latestPapers as $paper) {
                            // ex: "arxiv.org/abs/1902.10680 2019-02-27 analyzing the perceived severity of cybersecurity threats reported on social media"
                            $paperToAdd = "\r\n".$paper->url." ".$paper->publish_date." ".preg_replace('/\s+/', ' ', $paper->title);
			  
                        // dealing with pagination
                        if ( (strlen($messages[$messageCount]) + strlen($paperToAdd) + 10) > $maxTootChars ) { // keeping 10 characters for pagination
                        $messageCount += 1;
                        $messages[] = "@".$account['acct']."\r\n";
                        }

                        $messages[$messageCount] .= $paperToAdd;
                        }
                        
                        $i = 0;
                        foreach ($messages as $message) {
                            $messages[$i] .= "\r\n\r\n(".($i+1).'/'.count($messages).')';
                            $i += 1;
                        }

                    /////////////////
                    /// Commands, raw
                    /////////////////
                    } else {
                        // ex: "author k kording & author achakulvisut & title science & abstract recommendation"
                        //     "au:kording"
                        //     "ti:deep+AND+ti:learning"
                        //     "abs:%22deep+learning%22"

                        $command = $this->getApplication()->find('get:papers');
                        $arguments = [
                            'command' => 'get:papers',
                            '--days'  => 30,
                        ];
                    }

                    //Sending answer message (either explaining an error or sending a proper reply)
                    if ($error_message) {
			            $logger->info('sending error message: '.$error_message);
                        $this->curl($status['id'], $error_message);
                    } else {
			            $logger->info('sending message: '.$messages[0]);
			
			            $replyId = $status['id'];
                        foreach ($messages as $message) {
                            $curl = $this->curl($replyId, $message);
			    
                            $response = $curl->response;
                            $responseData = \json_decode($response, true);
                            $replyId = $responseData['id'];
                        }
                    }
                }
            }
        } catch (\ErrorException $e) {
            $logger->error('an error exception occurred:'.$e);
        } catch (\Exception $e) {
            $logger->error('an exception occurred:'.$e);
        }
    }

    /**
     * Deletes account data for a user that subscribed to new publications regarding their favourite category
     * @param $account account
     * @return string
     */
    private function delAccountData($account) {
        $act_db = $this->doctrine->getRepository("App:Account")->findOneBy(["uuid"=>trim($account['url'])]);
        if (!$act_db) {
            $message = "Hello  @".$account['acct']."! I didn't find data in relation with your account to delete :)";
        } else {
            $this->doctrine->getManager()->remove($act_db);
            $this->doctrine->getManager()->flush();
            $message =  "Hello @".$account['acct']."! All data in relation with your account have been removed :)";
        }
        return $message;
    }

    /**
     * Return a string cleaned of all HTML tags and all words beginning by '#' or '@'.
     * @param $string
     * @return string
     */
    private function cleanHTML($string) {
        $string = preg_replace('/<[^>]*>/', '', $string); // remove html tags
        $string = preg_replace('/(?<!\w)#\w+/', '', $string); // remove words beginning with a hashtag
        $string = preg_replace('/@([\S]+)/', '', $string); // remove words beginning with a at-sign
        $string = trim($string);
        $trans_tbl = get_html_translation_table(HTML_ENTITIES);
        $trans_tbl = array_flip($trans_tbl);
        return strtr($string, $trans_tbl);
    }

    /**
     * Return the curl object after making the request.
     * @param $in_reply_to_id
     * @param $message
     * @return curl object
     * @throws \ErrorException
     */
    private function curl($in_reply_to_id, $message) {
        $curl = new Curl();
        $curl->setHeader('Authorization', 'Bearer '. $this->app_token);
        $paramsPost['in_reply_to_id'] = $in_reply_to_id;
        $paramsPost['visibility'] = 'direct';
        $paramsPost['status'] = $message;
        $url = "https://" . $this->app_host . "/api/v1/statuses";
        $curl->post($url, $paramsPost);

        if (getenv('DEBUG') !== "1") return $curl;

        if ($curl->error) {
            echo $curl->error_code;
        }
        else {
            echo $curl->response;
        }

        var_dump($curl->request_headers);
        var_dump($curl->response_headers);
        return $curl;
    }

    /**
     * Return the number of characters per toot supported by the instance hosting the bot.
     * @return string
     * @throws \ErrorException
     */
    private function getMaxTootChars() {
        $curl = new Curl();
        $url = "https://" . $this->app_host . "/api/v1/instance";
        $curl->get($url);

        $response = $curl->response;
        $responseData = \json_decode($response, true);
        return $responseData['max_toot_chars'];
    }
}
