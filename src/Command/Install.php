<?php

namespace App\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class Install extends Command
{
    use LockableTrait;

    protected static $defaultName = 'install';
    private $container;
    private $doctrine;
    private $arxivCategories;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
        $this->doctrine = $container->get('doctrine');
    }

    protected function configure()
    {
        $this->setDescription('Installs most things: migrations, cron tasks, etc.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process. Can be run idempotently.');

            return 0;
        }

        $io = new SymfonyStyle($input, $output);
        $io->title('Setting your publicabot installation');

        $io->note('Initializing database');

        $consolepath = $this->container->getParameter('kernel.project_dir') . '/bin/console';

        $this->liveExecuteCommand( $consolepath . ' doctrine:database:create' );
        $this->liveExecuteCommand( $consolepath . ' doctrine:migrations:migrate --no-interaction' );

        $io->note('Initializing redis cache of papers. This might take a while.');

        $this->liveExecuteCommand( $consolepath . ' get:papers --days 30' );

        $io->note('Generated tasks for your cron in \'publicabot.cron\'. Add them to your user cron via \'crontab publicabot.cron\'');

        $filecontent = <<<EOT
        # Edit this file to introduce tasks to be run by cron.
        #
        # Each task to run has to be defined through a single line
        # indicating with different fields when the task will be run
        # and what command to run for the task
        #
        # To define the time you can provide concrete values for
        # minute (m), hour (h), day of month (dom), month (mon),
        # and day of week (dow) or use '*' in these fields (for 'any').#
        # Notice that tasks will be started based on the cron's system
        # daemon's notion of time and timezones.
        #
        # Output of the crontab jobs (including errors) is sent through
        # email to the user the crontab file belongs to (unless redirected).
        #
        # For example, you can run a backup of all your user accounts
        # at 5 a.m every week with:
        # 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
        #
        # For more information see the manual pages of crontab(5) and cron(8)
        #
        # m h  dom mon dow   command
        * * * * * php $consolepath read:notifications
        * * * * * sleep 5 && php $consolepath read:notifications
        * * * * * sleep 10 && php $consolepath read:notifications
        * * * * * sleep 15 && php $consolepath read:notifications
        * * * * * sleep 20 && php $consolepath read:notifications
        * * * * * sleep 25 && php $consolepath read:notifications
        * * * * * sleep 30 && php $consolepath read:notifications
        * * * * * sleep 35 && php $consolepath read:notifications
        * * * * * sleep 40 && php $consolepath read:notifications
        * * * * * sleep 45 && php $consolepath read:notifications
        * * * * * sleep 50 && php $consolepath read:notifications
        * * * * * sleep 55 && php $consolepath read:notifications
        5 22 * * * php $consolepath get:papers --days 30
        EOT;

        $fs = new Filesystem();
        $fs->appendToFile('publicabot.cron', $filecontent);
    }

    /**
     * Execute the given command by displaying console output live to the user.
     *  @param  string  cmd          :  command to be executed
     *  @return array   exit_status  :  exit status of the executed command
     *                  output       :  console output of the executed command
     */
    protected function liveExecuteCommand($cmd)
    {

        while (@ ob_end_flush()); // end all output buffers if any

        $proc = popen("$cmd 2>&1 ; echo Exit status : $?", 'r');

        $live_output     = "";
        $complete_output = "";

        while (!feof($proc))
        {
            $live_output     = fread($proc, 10000);
            $complete_output = $complete_output . $live_output;
            echo "$live_output";
            @ flush();
        }

        pclose($proc);

        // get exit status
        preg_match('/[0-9]+$/', $complete_output, $matches);

        // return exit status and intended output
        return array (
            'exit_status'  => intval($matches[0]),
            'output'       => str_replace("Exit status : " . $matches[0], '', $complete_output)
        );
    }
}
